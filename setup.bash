#!/bin/bash
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on

wget https://gitlab.surrey.ac.uk/js03432/cloud-computing-cw/-/blob/main/val_sim.py -P /var/www/cgi-bin
chmod +x /var/www/cgi-bin/val_sim.py
