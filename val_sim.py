#!/usr/bin/python3

import math, random, sys, json
from statistics import mean, stdev


event = json.loads(sys.stdin.read())


dt = eval(event['key1'])
close = eval(event['key2'])
buy = eval(event['key3'])
sell = eval(event['key4'])
h = int(event['key5'])
d = int(event['key6'])
t = event['key7']
minhistory = h
shots = d
var95_list = []
var99_list = []
dates = []

for i in range(minhistory, len(close)):
	if t == "buy":
		if buy[i] == 1:
			close_data = close[i-minhistory:i]
			pct_change = [(close_data[i] - close_data[i-1]) / close_data[i-1] for i in range(1,len(close_data))]
			mean_value = mean(pct_change)
			std_value = stdev(pct_change)
			simulated = [random.gauss(mean_value,std_value) for x in range(shots)]
			simulated.sort(reverse=True)
			var95 = simulated[int(len(simulated)*0.95)]
			var99 = simulated[int(len(simulated)*0.99)]
			var95_list.append(var95)
			var99_list.append(var99)
			dates.append(str(dt[i]))
	elif t == "sell":
		if sell[i] == 1: 
			close_data = close[i-minhistory:i]
			pct_change = [(close_data[i] - close_data[i-1]) / close_data[i-1] for i in range(1,len(close_data))]
			mean_value = mean(pct_change)
			std_value = stdev(pct_change)
			simulated = [random.gauss(mean_value,std_value) for x in range(shots)]
			simulated.sort(reverse=True)
			var95 = simulated[int(len(simulated)*0.95)]
			var99 = simulated[int(len(simulated)*0.99)]
			var95_list.append(var95)
			var99_list.append(var99)
			dates.append(str(dt[i]))

output = {"dates" : dates,
		"var95" : var95_list,
		"var99" : var99_list
		}

output = json.dumps(output)

print("Content-Type: application/json")
print()
print(output)
